# GreyNoise

Some useful PowerShell for using the Greynoise API v1.

# How do I use it?

Just clone the repository. Greynoise's public API lets you scrape up to 500 results without authenticating. You can contact them for an API key if you need more results than that. 

This has been tested on Powershell Core 6.3 

## List all GreyNoise tags

./poshNOISE.ps1 -a list

## List all tags for a given IP

./poshNOISE.ps1 -a ip -t 169.254.13.37

## List all IP's for a given tag

./poshNOISE.ps1 -a tag -t "SHODAN"


# Save results to a text file 

./poshNOISE.ps1 -a ip -t 169.254.13.37 | Out-File -Path "PATH-HERE"

# API Documentation

https://github.com/GreyNoise-Intelligence/api.greynoise.io