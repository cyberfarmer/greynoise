param([String] $a, $t)
Write-Host "
 ██▓███   ▒█████    ██████  ██░ ██     ███▄    █  ▒█████   ██▓  ██████ ▓█████ 
▓██░  ██▒▒██▒  ██▒▒██    ▒ ▓██░ ██▒    ██ ▀█   █ ▒██▒  ██▒▓██▒▒██    ▒ ▓█   ▀ 
▓██░ ██▓▒▒██░  ██▒░ ▓██▄   ▒██▀▀██░   ▓██  ▀█ ██▒▒██░  ██▒▒██▒░ ▓██▄   ▒███   
▒██▄█▓▒ ▒▒██   ██░  ▒   ██▒░▓█ ░██    ▓██▒  ▐▌██▒▒██   ██░░██░  ▒   ██▒▒▓█  ▄ 
▒██▒ ░  ░░ ████▓▒░▒██████▒▒░▓█▒░██▓   ▒██░   ▓██░░ ████▓▒░░██░▒██████▒▒░▒████▒
▒▓▒░ ░  ░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒ ░░▒░▒   ░ ▒░   ▒ ▒ ░ ▒░▒░▒░ ░▓  ▒ ▒▓▒ ▒ ░░░ ▒░ ░
░▒ ░       ░ ▒ ▒░ ░ ░▒  ░ ░ ▒ ░▒░ ░   ░ ░░   ░ ▒░  ░ ▒ ▒░  ▒ ░░ ░▒  ░ ░ ░ ░  ░
░░       ░ ░ ░ ▒  ░  ░  ░   ░  ░░ ░      ░   ░ ░ ░ ░ ░ ▒   ▒ ░░  ░  ░     ░   
             ░ ░        ░   ░  ░  ░            ░     ░ ░   ░        ░     ░  ░
                                                                              
                                                                                        GreyNoise API Wrapper by blackhats.ca
" -ForegroundColor Magenta

switch ($a) {

	list{
		Write-Host "List GreyNoise Tags (Current)" -ForegroundColor Magenta
        $gn_results = Invoke-RestMethod -Uri "http://api.greynoise.io:8888/v1/query/list" -Method GET
        $gn_results | Out-String
	}
    tag {
        Write-Host "Query all IP's with the GreyNoise Tag: $t" -ForegroundColor Magenta
        $params = @{'tag'=$t}                                                                                            
        $gn_results = Invoke-RestMethod -Uri "http://api.greynoise.io:8888/v1/query/tag" -Method POST -Body $params
        $gn_results | Out-String

    }
    ip {
        Write-Host "Query all tags associated with the IP: $t" -ForegroundColor Magenta
        $params = @{'ip'=$t}                                                                                            
        $gn_results = Invoke-RestMethod -Uri "http://api.greynoise.io:8888/v1/query/ip" -Method POST -Body $params
        $gn_results | Out-String
    }


}

exit(0)
